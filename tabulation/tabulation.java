import java.util.Scanner;
import static java.lang.Math.*;
public class tabulation {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("lower bound:");
		double i = in.nextDouble();
		System.out.print("upper bound:");
		double j = in.nextDouble();
		System.out.print("step tabulation:");
		double k = in.nextDouble();
		while (i<=j) {
		System.out.println("for " + i + ":  " + Math.sin(i));
		i = i + k;
		}
	}
}